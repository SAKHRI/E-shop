<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shoppingCart", cascade={"persist", "remove"})
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductLine", mappedBy="shoppingCart", cascade={"persist"})
     */
    private $ProductLine;

    public function __construct()
    {
        $this->ProductLine = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|ProductLine[]
     */
    public function getProductLine(): Collection
    {
        return $this->ProductLine;
    }

    public function addProductLine(ProductLine $productLine): self
    {
        if (!$this->ProductLine->contains($productLine)) {
            $this->ProductLine[] = $productLine;
            $productLine->setShoppingCart($this);
        }

        return $this;
    }

    public function removeProductLine(ProductLine $productLine): self
    {
        if ($this->ProductLine->contains($productLine)) {
            $this->ProductLine->removeElement($productLine);
            // set the owning side to null (unless already changed)
            if ($productLine->getShoppingCart() === $this) {
                $productLine->setShoppingCart(null);
            }
        }

        return $this;
    }
}
