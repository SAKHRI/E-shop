<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ShoppingCart;
use App\Entity\ProductLine;
use Symfony\Component\HttpFoundation\Request;

class ProductLineController extends Controller
{
    /**
     * @Route("/user/product-line/{id}", name="product_line")
     */
    public function index( Session $session = null, Request $request, int $id, ProductRepository $repo, ObjectManager $manager)
    {
        $user = $this->getUser();
        $number = $request->get("number");
        $cart = $user->getShoppingCart();

        if(!$cart) {
            $cart = new ShoppingCart();
        } else {
            $cart = $manager->merge($cart);
           
        }

        $productLine = new ProductLine;

        $repo = $repo->find($id);

        
        $productLine->setProduct($repo);

        $productLine->setQuantity($number);
        
        $price = $repo->getPrice() * $number;

        $productLine->setPrice($price);

        $cart->addProductLine($productLine);

        $cart->setUser($user);
        
        
        $manager->persist($cart);
        $manager->flush();

        return $this->redirectToRoute("home", []);
    }

    /**
    *  @Route("/user/remove-productLine/{id}", name="remove_productLine")
    */
    public function remove(ProductLine $productLine) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($productLine);
        $em->flush();

        dump($this->getUser());

        return $this->redirectToRoute("shopping_cart", []);
    }

}